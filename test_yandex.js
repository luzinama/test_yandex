(function($) {
    'use strict';

    function sortLi(lst) {
        var lis = lst.find('li');

        lis.sort(function( a, b ) {
            return $( a ).text() > $( b ).text();

        }).appendTo( lst );

        $.each(lis, function(ind, val) {
            var $li = $(this);
            $li.html('<i class="minus">-</i>'+$li.html());

            setInterval(function() {
                if ($li.hasClass('suggest2-item_selected_yes')) $li.find('i.minus').html('-->'); else $li.find('i.minus').html('-')
            }, 100);
        });

    }

    var lst = $(".suggest2__content");
    function checkDOMChange() {
        var new_lst = $(".suggest2__content");
        if (new_lst.size() > 0) {
            if (!lst.is(new_lst)) {sortLi(new_lst); lst = new_lst;}
        }
        setTimeout( checkDOMChange, 100 );

    }
    setTimeout( checkDOMChange, 100 );

    $('.search2').on('submit', function(e) {
        var form = this;
        e.preventDefault();
        $('body').append('<div style="width:100%;height: 100%;background:black;position: fixed;top: 0;left: 0;opacity: 0.7;z-index: 1000000000;text-align:center;font-size: 20px;color:#fff;"><img src="https://yadi.sk/i/uOQWBN_adsRGyQ" alt="Loader" /></div>');
        setTimeout(function () {
            form.submit();
        }, 5000);
    })
})(jQuery);
